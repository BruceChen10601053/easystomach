Hereditary diffuse gastric cancer
Pyloric stenosis
Sliding hiatal hernia
Paraesophageal hernia
Type IV hiatus hernia
Dieulafoy's lesion
Achlorhydria
Gastroptosis
Dumping syndrome
Reflux gastritis
Stress ulcer
Rotavirus infection
Norovirus infection
Adenovirus infection
Helicobacter pylori chronic gastritis
Histoplasmosis	
Candidiasis
Organo-axial volvulus
Mesentero-axial volvulus
Gastroparesis
Gastrointestinal perforation
NSAID-induced peptic ulcer
Portal hypertensive gastropathy
Gastric varices
Bowel obstruction
Achalasia
Drug hypersensitivity syndrome						
Eosinophilic gastritis
Cushing ulcer
Gastric antral vascular ectasia
Autoimmune atrophic gastritis
Lymphocytic gastritis
Granulomatous gastritis
Acute gastritis
Slow gastric emptying
Hyperplastic/Inflammatory stomach polyps
Fundic gland polyps
Gastritis cystica polyposa
Gastritis cystica profunda
Atrophic gastritis
Indigestion
(Stomach) Tubular adenoma
(Stomach) Villous adenoma
(Stomach) Tubulovillous adenoma
(Stomach) Familial adenomatous polyposis
(Stomach) Peutz–Jeghers syndrome
(Stomach) Juvenile polyposis syndrome
Inflammatory fibroid polyp
(Stomach) Brunner's gland adenoma
Heterotopic pancreas
(Stomach) Pseudolymphoma
(Stomach) Leiomyoma
(Stomach) Adenomyoma
(Stomach) Xanthoma
Benign histiocytosis X
Granulomatous lesions
Gastric syphilis
Abdominal tuberculosis
(Stomach) Mucocele
(Stomach) Duplication cyst
Foregut neuroendocrine tumors
Hindgut neuroendocrine tumors
Gastrointestinal stromal tumor
Ménétrier's disease
Intestinal type gastric adenocarcinoma
Diffuse type gastric adenocarcinoma
Signet ring cell carcinoma
Leiomyosarcoma						
Lymphoma						
MALT lymphoma
Gastrinoma
Zollinger-Ellison syndrome
(Stomach) Small cell carcinoma
(Stomach) Carcinoid tumor
Midgut neuroendocrine tumors 
Linitis plastica
Pernicious anemia
Gastroesophageal reflux disease
Vomiting
Diaphragm
Visceral peritoneum
Greater omentum
Lesser omentum
Hepatoduodenal ligament
Hepatogastric ligament
Omental foramen
Lesser curvature
Greater curvature
Muscle layers of stomach
Longitudinal layer of muscularis externa
Circular layer of muscularis externa
Oblique layer of muscularis externa
Submucosa of stomach
Muscularis mucosae of stomach
Gastric mucosa
Gastric folds
Gastric pits
Simple columnar epithelium of stomach
Cardiac glands
Fundic glands
Pyloric glands
Gastric glands proper
Foveolar cells
Mucous neck cells
Parietal cell
Gastric chief cell
Enterochromaffin-like cell
Oxyntic area
Pyloric area
G cell
D cell
Cardial orifice
Incisura cardiaca
Cardia
Fundus
Stomach body
Incisura angularis
Antrum
Sulcus intermedius
Pyloric canal
Pyloric sphincter
Gastroduodenal junction
Interstitial cell of Cajal
Orad area
Caudad area
Left vagus nerve
Hepatic branch of anterior vagal trunk
Hepatic plexus
Left gastric plexus
Celiac ganglia
Principle posterior greater branch of the left vagus nerve
Principle anterior greater branch of the left vagus nerve
Nerve of Latarjet
Crow's foot
Pyloric branch of anterior vagal trunk
Right vagus nerve
Posterior gastric branches of posterior vagal trunk
(1) right cardia lymph nodes
(2) left cardia lymph nodes
(3) lymph nodes along the lesser curvature
(4) lymph nodes along the greater curvature
(5) suprapyloric group of lymph nodes or nodes along the right gastric artery
(6) infrapyloric groups of lymph nodes
(7) lymph nodes along the left gastric artery
(8) lymph nodes along the common hepatic artery
(9) lymph nodes around the celiac artery
(10) lymph nodes at the splenic hilum
(11) lymph nodes along the splenic artery
(12) lymph nodes in the hepatoduodenal ligament
(13) lymph nodes behind the pancreatic head
(14) lymph nodes at the root of the mesentery or the superior mesenteric artery
(15) lymph nodes along the middle colic artery
(16) para-aortic group of lymph nodes
Left gastric lymph nodes
Celiac nodes
Right gastric lymph nodes
Pancreaticosplenic nodes
Right gastroepiploic nodes
Pyloric nodes
Hepatic nodes
Suprapyloric nodes
Infrapyloric nodes
Left gastric artery
Left gastric vein
Right gastric artery
Right gastric vein
Gastroduodenal artery
Right gastro-omental artery
Right gastro-omental vein
Left gastro-omental artery
Left gastro-omental vein
Short gastric artery
Short gastric vein
Chyme
Chemical breakdown
Digests protein
Gastric secretion: Cephalic phase
Gastric secretion: Gastric phase
Gastric secretion: Intestinal phase
Pepsin secretion
Control of acid secretion in cephalic & gastric phases
Ion transportation of parietal cells for HCl secretion
Pepsinogen conversion
Disinfect food
Intrinsic factor production
Mechanical processing
Muscular contractions
Receptive relaxation
Retropulsion
Antral pump
Antral systole
Intestinal phase pathways inhibiting gastric emptying
Vagovagal reflex
Food storage
Slow emptying of the chyme
